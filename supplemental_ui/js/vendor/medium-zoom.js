;(function () {
  var Mzm = (window.mzm = require('medium-zoom/dist/medium-zoom.min'))
  /// then we need to add role to each image like
  /// image::myimage.png['this is my image',role="zoom"] 

  // Mzm('span.frame img, span.zoom img', { background: '#fff' , margin: 10 })

  /// If you want that all images are zoomable without extra definition if zoomable or not, change:
  /// 'span.image img'
  ///    css selector that medium-zoom connects for standard images like jpg or png
  /// 'div.imageblock img'
  ///    css selector that medium-zoom connects for svg images
  /// with that, there is no need to define a role in the image for zooming - all images are zoomable !

  Mzm('span.image img, div.imageblock img, span.img object', { background: '#fff', margin: 10 })

})()
