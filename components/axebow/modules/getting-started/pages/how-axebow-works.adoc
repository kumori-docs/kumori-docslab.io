= How Axebow works?
:note-number: 01-pre
:revnumber: {note-number}
:api-version: 1.0-beta
:icons: font
:toclevels: 4
:icons: font
:sectanchors:
:sectlinks:
:sectnums:
:imagesdir: ./assets/images
:toc-title: Contents
:toc: right

image::Axebow_w800.png[Axebow Tech Stack,600]

The above image shows the tech stack which are being used by Axebow: 

* *axebow UI*, clean web used interface for developers and operators.
* *CMC API*, a robust REST API for DevOps.
* *Kumori PaaS*, the crown jewel, our own Kubernetes based PaaS, aka Kumori Platform.
* *Marketplace* of services and components.

== Axebow UI

Axebow web UI has been crafted with user-friendliness and efficiency in mind, ensuring that you can easily navigate through its features and functionalities.

Key Features:

* *Dashboard* Oriented: Divided into three dashboards (Admin, Developer and Marketplace) allowing the user to focus on the information that matters.

* *User-Friendly* Navigation: A left-side menu provides easy access to all major sections of the platform. Whether you need to manage your profile, access services, or configure infrastructure, everything is just a click away.

* *Responsive* Design: The web UI is fully responsive, ensuring a consistent and optimal experience across all devices, whether you're using a desktop, tablet, or smartphone.

* *Interactive* Elements: Engage with real-time data updates, and dynamic content that keep you informed and in control.

* *Help* and *Support*: Need assistance? The help dialog is integrated into the UI, providing quick access to FAQs, tutorials, and customer support.

Explore xref:the-web-interface.adoc[the web UI] to know how to achieve Developer and DevOps goals.

== CMC API

The CMC API is the specification of the service that the Axebow platform exposes to its users (mostly excluding operators of the platform itself). It is in charge of attending requests to manage the Axebow entities like services,  resources, users and tenants (see xref:basic-concepts.adoc[Basic concepts]).

The CMC API is HTTP-based, routing different actions based on the request's URL and the HTTP method being used.

Axebow authenticates its users by the use of mTLS client certs or through a set of federated ID providers. All calls to the API (except the call to get the information about Axebow) must be authenticated.

See the xref:the-rest-api.adoc[CMC API detailed guide] for full understanding.

== Kumori PaaS

Kumori Platform is a Platform as a Service (PaaS) built on top of the open source Kumori Kubernetes distribution.

As a platform it simplifies and streamlines many aspects of building, deploying and managing a service application whose scalability is automatically managed by the platform itself based on a series of indications provided by the service integrator/developer, mainly the SLA expressed as an expected response time within a range of load.

Read the xref:kpaas:ROOT:index.adoc[Kumori Platform] document to learn more about the platform.

== Marketplace

The Marketplace is a key component in the Platform Engineering practices because it enables to improve reusability, security, and quality.

As https://www.gartner.com/en/information-technology/insights/top-technology-trends[Gartner,role=external,window=_blank] said, by 2026, 80% of software engineering organizations will establish platform teams as internal providers of reusable services, components, and tools for application delivery

The Marketplace is inside the Axebow UI. Learn more about xref:the-web-interface.adoc#marketplace-dashboard[the Marketplace] features.

== Cloud Providers

The organizations are increasingly using multiple private and public clouds to deploy applications.

But multiple clouds can be difficult to manage. It means multiple vendors and multiple costs. Connectivity takes too long. Your ecosystem has spiraled out of control and governance is no longer clear.

Kumori Platform is built to work on OVH, AWS, GCP, Azure and any cloud provider, solving all these problems by adding a unique connectivity layer that joins up a multi-cloud ecosystem.

The Axebow UI provides a homogeneus way to manage the infrastructure in different cloud providers. Learn more about an xref:basic-concepts.adoc#account[Account], the cloud provider entry on Axebow and how to xref:the-web-interface.adoc#new-account[create account] to gain access to any cloud provider.

