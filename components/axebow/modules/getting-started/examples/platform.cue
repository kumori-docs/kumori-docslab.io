package axebow

#ProtocolVersion: =~ "^\\d+\\.\\d+(-(pre|rc|alpha|beta)\\d)?$"
#Version: =~ "^\\d+\\.\\d+\\.\\d+(-(pre|rc|alpha|beta)\\d)?$"
#Limits: {...}

// tag::info[]
#PlatformInfo: {
  apiVersions    : [...#ProtocolVersion],   // <1>
  instanceName   : string,                  // <2>
  platformVersion: #Version,                // <3>
  oidProviders: string[]                    // <4>
  iaasProviders: [provider:string]: {...}   // <5>
  freemiumAdvancedLimits: #Limits           // <6>
  freemiumLimits: #Limits                   // <7>
  portRange: number[]                       // <8>
  referenceDomain: string                   // <9>
}
// end::info[]