package axebow

// tag::info[]
#TenantInfo: this={
    accounts: [account=string]: {
        account     : #TenantAccount
        environments: [string]: #EnvironmentSpec & {_account: account, account: this.account}
    }
    users: [string]: {
        role     : #TenantUserRole
        confirmed: bool
    }
    resources: [kind=#ResourceKind]: [string]: #ResourceSpec[kind]
    rules: #TenantSpec.rules
}
// end::info[]

// tag::spec[]
#TenantSpec: {
    rules: {
        // Selecting an environment to deploy a service
        selectEnvironmentService      : [...{reason: string, sel: #Selector}]

        // Selecting an environment with explicit acceptance
        selectAcceptEnvironmentService: [...{reason: string, sel: #Selector}]

        // Validates an environment to place a resource within
        validateEnvironmentResource   : [...{reason: string, sel: #Selector}]

        // Validates an environment for a User to deploy there
        validateEnvironmentUser       : [...{reason: string, sel: #Selector}]
    },
    registry: {
      endpoint     : string,      // <1>
      credentials? : string,      // <2>
      domain       : string,      // <3>
      public       : *true | bool // <4>
    }
}
// end::spec[]