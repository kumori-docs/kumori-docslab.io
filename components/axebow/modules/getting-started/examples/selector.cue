package axebow


// Selection of environments for resources and deployments
// and validation of environments for users



#Ops: "::getor" | // 2 => 1:  gets the reference on top, if it exists. 
                  // Else returns the next element
  "::exists"    | // 1 => 1:  returns true is the reference exists, false otherwise
  "::dup"       | // 1 => 2:  duplicate top of the stack
  "::pop"       | // 1 => 0:  pops top value
  "::swap"      | // 2 => 2:  Swap top two values on the stack
  "::num"       | // 1 => 1:  Convert to decimal number or leave a 0
  "::bool"      | // 1 => 1:  Convert to boolean (anything not null or false is true)
  "::eq"        | // 2 => 1:  Is equal
  "::neq"       | // 2 => 1:  Is not equal
  "::gt"        | // 2 => 1:  Is greater than
  "::gte"       | // 2 => 1:  Is greater than or equal
  "::lt"        | // 2 => 1:  Is less than
  "::lte"       | // 2 => 1:  Is less than or equal
  "::not"       | // 1 => 1:  Logical not
  "::and"       | // 2 => 1:  Logical and
  "::nand"      | // 2 => 1:  Logical nand
  "::or"        | // 2 => 1:  Logical or
  "::nor"       | // 2 => 1:  Logical nor
  "::xor"         // 2 => 1:  Logical xor


#Ref   : =~ "^\\$\\.(this|user|service|resource|environment)(\\.[^.]+)+$"
#NotRef: !~ "^\\$\\."
#NotOps: !~ "^::"

#string: #Ref | #Ops | (#NotOps & #NotRef)

#SelectorItem: #string | number | bool | null 

#Selector_Item_Resource: #SelectorItem & !~ "^\\$\\.(user|service|resource)"
#Selector_Item_User    : #SelectorItem & !~ "^\\$\\.(user|resource)"
#Selector_Item_Service : #SelectorItem & !~ "^\\$\\.(user|resource|service)" 

#Selector: [...#SelectorItem]
 
/**
  A selector is a stack program evaluating to a boolean

  A selector item can be data, a reference (special kind of data) or an operation

  Operations act on the top of the stack and consume the value therein, potentially 
  producing a new value that gets pushed.

  When evaluated, the value on top of the stack is the result of the evaluation.
  If evaluation produces an error (e.g., conversion errors to num or bool)

  Evaluation of a selector occurs in a context from which some objects
  can be references for values out of a JSON structure for the object

  Selectors are represented as CUE arrays constrained as shown below

  References in a selector MUST be to only 
   $this
   $user
   $environment
   $deployment
   $resource

   objects, and must consist of paths into them.
   Those paths can reach into any of the structures of those objects as defined
   in each objects INFO section (info: structure returned with a get info op)

   The $this object will always exist and will represent the object on which
   the selector has been defined.

   The rest of the objects may, or may not exist, depending the kind of object
   $this is. For each case where a selector can be specified, the context objects available
   will be defined.
*/



#Selector_Resource: [...#Selector_Item_Resource]
#Selector_User    : [...#Selector_Item_User    ]
#Selector_Service : [...#Selector_Item_Service ]

a: #Selector & [1, 2, "$.this.that.there.where", "::dup", "::getor"]


