package axebow

// tag::spec[]
#ServiceSpec: {
    bundle: ZipFile
    // Arbitrary JSON used for environment selection as well as 
    // later on, for scaling decisions.
    // Note that his metadata is not available within the service being
    // deployed.
    meta   : {...}

    // Not available for decisions
    comment: string
}
// end::spec[]

// tag::info[]
#ServiceStatus: {
    // ID of the first revision
    firstRevision   : number
    currentRevision : number
    history         : [...number]
    environment    ?: string 
}

#SerficeInfo: {
    spec  : #ServiceSpec
    status: #ServiceStatus
}
// end::info[]

// tag::revspec[]
#RevisionSpec: {
    service: string
    previousRevision: uint 
}
// end::revspec[]

// tag::revinfo[]
#RevisionStatus: {
    accepted: bool 
    resources: : [#ResourceKind]: [...string]
    usage: {
        max: {
            vcpu: uint
            ram:  uint  // in MB
        }
        min: {
            vcpu: uint
            ram:  uint  // in MB
        }
    }
}

#RevisionInfo: {
    spec  : #RevisionSpec
    status: #RevisionStatus
}
// end::revinfo[]