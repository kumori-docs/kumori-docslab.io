package axebow

// tag::spec[]
#EnvironmentSpec: this={
    // Marks are used to define limits in resource usage.
    marks: #Marks
    // Arbitrary metadata, including any kind of quota info.
    labels?: {...}      
}
// end::spec[]

// tag::info[]
// The status of an environment shows dynamic data like the number of vsrious
// resources in used, and for vcpus, their accumulated usage.
#MarksStatus: {
  vcpu:      {current: number, unit: "m"}
  memory:    {current: number, unit: "MB"}
  vstorage:  {current: number, unit: "MB"}
  nrstorage: {current: number, unit: "MB"}
  rstorage:  {current: number, unit: "MB"}
  storage:   {current: number, unit: "MB"}
  cost:      {current: number, unit: "EUR"}
}

#EnvironmentInfo: {
    spec  : #EnvironmentSpec
    status: {
      marks: #MarksStatus
    }
}
// end::info[]


