package axebow

#ResourceKind: "ca" | "certificate" | "domain" | "port" | "secret" | "volume"

#ResourceId: [id_kind=#ResourceKind]: {
  name: string
  kind: id_kind
  parent: {
    name: string
    kind: "tenant"
  }
}

#Resource: [kind=#ResourceKind]: {
  id               : #ResourceId[kind]   // The id of the resource
  spec             : #ResourceSpec[kind] // The actual spec of the resource
  meta?            : [string]: string    // Optional dictionary of values
  selector?        : #SelectorResource   // Optional selector of the environment
  status           : {                   // list of cluster/accounts/environments where the resources registered/used
    clusters: [string]: bool
    requested: [string]: number
    environment: [string]: number
  }
}

#ResourceSpec: [kind=#ResourceKind]: {
    data     : #ResourceData[kind] // Actual data spec to be provided when creating
}

// Dictionary of resource data structures
#ResourceData: {
    ca         : #Data_CA
    certificate: #Data_Certificate
    domain     : #Data_Domain
    port       : #Data_Port
    secret     : #Data_Secret
    volume     : #Data_Volume
}


#Data_CA: string

#Data_Certificate: {
  cert: string,           // String representation of a cert
  key:  string,           // String representation of the private key
  domain: #DomainString   // A string representing a domain name
}

#Data_Domain: #DomainString

#Data_Port: uint16

#Data_Secret: string

#Data_Volume: {
  items: uint                    // How many subvolumes
  size: #Magnitude               // a magnitude string, e.g. 10M, 5G
  type: "replicated" | "single"  // kind of volumes
  provider: string               // CSI provider, as named by the platform
}
