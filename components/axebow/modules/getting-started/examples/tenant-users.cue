package axebow

#TenantUserRole: "plain" | "admin" | "owner"

// A tenant user selector is employed to determine the 
// environments it can deploy a service to.
// Note that selectors make sense only for non-admin users, as admin users can 
// always modify that property
#TenantUserSpec: {
    user      : string           // User Name
    role      : #TenantUserRole
    meta     ?: {...}
    selector ?: #Selector_User
}

#TenantUserInfo: {
    spec     : #TenantUserSpec
    confirmed: bool
}

