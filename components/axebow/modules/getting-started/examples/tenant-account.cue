package axebow


// An IaaS provider info needs to specify at least 
// the set of flavors for storage and vms 
// that the IaaS provides and the tenant is interested in
// The names will map to actual IaaS flavors as dictated by Axebow

#UnimplementedProvider: {
    vmFlavors:      [...string]
    storageFlavors: [...string]
}



#IaaSProviderInfo_Aws  : #UnimplementedProvider
#IaaSProviderInfo_OVH  : #UnimplementedProvider
#IaaSProviderInfo_GOOG : #UnimplementedProvider
#IaaSProviderInfo_Azure: #UnimplementedProvider
#IaaSProviderInfo_Aire : #UnimplementedProvider
#IaaSProviderInfo_Gigas: #UnimplementedProvider
#IaaSProviderInfo_Arsys: #UnimplementedProvider

#IaaSProviderInfo: {
    aws   : #IaaSProvider_Aws
    ovh   : #IaaSProvider_OVH
    google: #IaaSProvider_GOOG
    azure : #IaaSProvider_Azure
    aire  : #IaaSProvider_Aire
    gigas : #IaaSProvider_Gigas
    arsys : #IaaSProvider_Arsys
}

#IaaSProviderKeys: [for k,v in #IaaSProviderInfo {k}]
#IaaSProviderKey : or(#IaaSProviderKeys)

// tag::account[]
#TenantAccount: this={
    spec : {
      iaas          : #IaaSProviderKey
      iaasInfo      : #IaaSProviderInfo[this.iaas]
      marks         : #Marks
    }
    meta: {
      labels: [string]: string
    }
}
// end::account[]

#TenantAccount: this={
    iaasInfo: _ 
    #VMFlavor     : or(this.iaasInfo.vmFlavors)
    #StorageFlavor: or(this.iaasInfo.storageFlavors)
}