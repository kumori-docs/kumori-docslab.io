package axebow


// tag::request[]
#CapableKinds: "channel" | "domain" | "port" | "ca" | "secret" | "certificate"

#CapabilityRequest: {
  tenant: string  // tenant that can consume the capability
  object: {
    kind: #CapableKinds
    name: string    // format is <servicename>/<serverchannelname> for channels
  }
  ttl:    uint    // for how long is this capability valid, in milliseconds
  metadata: {...}   // Any json data TBD potentially dependent on the 
                    // kind of capability.
                    //  E.g., it may be the SLA for a server channel
}
// end::request[]

// tag::response[]
#CapabilityContent: {
    ownerTenant   : string
    borrowerTenant: string
    kind          : #CapableObject
    id            : string
    validityTstamp: uint
}

#CapabilityResponse: {
    token: jwt(#CapabilityContent, axebow)  // pseudo signing function
}
// end::response[]