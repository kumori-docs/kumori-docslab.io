#OIdPProvider: "google" | "github" | "gitlab" | "axebow"

#BaseProfile: {

}

#OIdPProfile_google: {#BaseProfile,...}
#OIdPProfile_github: {#BaseProfile,...}
#OIdPProfile_gitlab: {#BaseProfile,...}
#OIdPProfile_azure : {#BaseProfile,...}

#OIdPProfile: {
    google: #OIdPProfile_google
    github: #OIdPProfile_github
    gitlab: #OIdPProfile_gitlab
    azure : #OIdPProfile_azure
}

// tag::profile[]
#UserProfile: {
  provider: #OIdPProvider,          // name of provider for Axebow
  profile : #OIdPProfile[provider]  // Profile structure for the provider
}
// end::profile[]

// tag::info[]
#UserInfo: {
  tenants: {
    [string]: #TenanUserRole
  }
  profiles: {
    [oidp=string]: #OIdPProfile[oidp]
  }
}
// end::info[]