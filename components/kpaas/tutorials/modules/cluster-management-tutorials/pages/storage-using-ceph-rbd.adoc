= Kumori cluster: Using Ceph RBD storage
Kumori Systems S.L. <info@kumori.systems>
v1.4.0, Feb 2024
:title-page:
:compat-mode:
:doctype: article
:experimental:
:icons: font
:sectanchors:
:sectlinks:
:sectnums:
:toc:
:toclevels: 3
:imagesdir: ./assets/images

Kumori Cluster can be configured to provide volumes backed by an existing Ceph RBD pool.
This document describes how to configure the CSI Ceph RBD addon in Kumori Clusters and provides
some helpful commands for an Operator to connect to the Ceph pool to monitor the existing RBD
images. The final part of this document describes a node failure scenario and how a Pod using
a Ceph RBD-backed volume is handled.


== Ceph cluster configuration (previous step)

Prior to using the CSI Ceph RBD provider, an RBD pool must created in the Ceph cluster and a user
must exit with access to that pool.

The necessary data to gather from the Ceph cluster and the pool is:

* the Ceph cluster ID
* the list of monitors of the Ceph cluster
* the pool name
* the pool user username (ID)
* the pool user password (key)

NOTE: the following commands are just for reference and requires the `ceph` CLI tool to be
installed properly configured.

The Ceph cluster ID can be obtained by running the following command:
----
$ ceph fsid

12345678-1234-1234-1234-112233445566
----

To get the list of monitors, run:
----
$ ceph mon dump

dumped monmap epoch 26
epoch 26
fsid 12345678-1234-1234-1234-112233445566
last_changed 2023-11-13T09:06:15.675921+0000
created 2021-10-20T12:53:39.592600+0000
min_mon_release 17 (quincy)
election_strategy: 1
0: [v2:172.16.2.102:3300/0,v1:172.16.2.102:6789/0] mon.cephtest01b
1: [v2:172.16.2.103:3300/0,v1:172.16.2.103:6789/0] mon.cephtest01c
2: [v2:172.16.2.104:3300/0,v1:172.16.2.104:6789/0] mon.cephtest01d
----


== Cluster configuration

=== CSI Ceph RBD provider configuration

To enable CSI Ceph RBD storage provider in a Kumori Cluster, it must be enabled when configuring
the cluster. In the `config.cue` file the necessary Ceph cluster and pool configuration settings
must be provided, and a list of classes configured.

[source%autofit,cue]
----
"csi-ceph-rbd": {
  enabled: true                                                 <1>
  HA: true                                                      <2>
  cephClusters: {                                               <3>
    "cluster-01": {                                             <4>
      clusterID: "12345678-1234-1234-1234-111111111111"         <5>
      monitors: [                                               <6>
        "192.168.0.10:6789",
        "192.168.0.11:6789",
        "192.168.0.12:6789"
      ]
    }
    "cluster-02": {
      clusterID: "12345678-1234-1234-1234-222222222222"
      monitors: [
        "192.168.0.20:6789",
        "192.168.0.21:6789",
        "192.168.0.22:6789"
      ]
    }
    "cluster-03": {
      clusterID: "12345678-1234-1234-1234-333333333333"
      monitors: [
        "192.168.0.30:6789",
        "192.168.0.31:6789",
        "192.168.0.32:6789"
      ]
    }
  }
  // Classes of storage that will be available for backing user volume types
  classes: {                                                    <7>
    "cluster01-pool1": {                                        <8>
      configuration: {                                          <9>
        clusterID: "12345678-1234-1234-1234-111111111111"       <10>
        poolName: "rbd-pool-1"                                  <11>
        poolUserID: "username"                                  <12>
        poolUserKey: "password"                                 <13>
        fsType: "ext4"                                          <14>
      }
      properties: "volatile"                                    <15>
    }
    "cluster01-pool2": {
      configuration: {
        clusterID: "12345678-1234-1234-1234-111111111111"
        poolName: "rbd-pool-2"
        poolUserID: "username2"
        poolUserKey: "password2"
        fsType: "ext4"
      }
      properties: "persistent"
    }
    "cluster02-pool1": {
      configuration: {
        clusterID: "12345678-1234-1234-1234-222222222222"
        poolName: "rbd-pool-1"
        poolUserID: "username3"
        poolUserKey: "password3"
        fsType: "ext4"
      }
      properties: "persistent"
    }
  }
}
----
<1>  Enable or disable the installation of the CSI Ceph RBD storage provider in the cluster
<2>  Set to true to deploy the CSI Ceph RBD provider in High Availability
<3>  List of the Ceph clusters that be accessed with the CSI Ceph RBD provider
<4>  A name for the cluster (only used for making configuration clearer)
<5>  For each Ceph cluster, its cluster ID
<6>  For each Ceph cluster, the list of its monitors
<7>  List of classes of storage that will be connected to the CSI Ceph RBD provider
<8>  A name of the class, for referring to it when defining the cluster volume types
<9>  Configuration section (CSI Ceph RBD-specific)
<10> For each class, the ID of the Ceph cluster it connects to
<11> For each class, the name of the Ceph pool it connects to
<12> For each class,the user ID for connecting to the pool
<13> For each class, the user key (password) for connecting to the pool
<14> For each class, the filesystem type the provided volumes will use
<15> Properties section. Available properties are: `persistent` or `volatile` (mututally exclusive),
     and `shared`. These properties determine what the life-cycle of the provided volumes will be.


=== Cluster volume types

Besides configuring CSI providers and the classes of storage they offer, a list of volume types
that will be available for users must be configured.

[source%autofit,cue]
----
// Configuration of the volume types that will be available to users
// Each volume type has a name (the map key) and must include:
// - a provider: be one of the configured Storage providers or "" (volumes
//   will be provided using temporary directories)
// - a class: a class configured in the selected provider. If no provider
//   is set, it should be set to "" (it will be ignored)
volumeTypes: {
  "ceph-c01p01": {                <1>
    provider: "csi-ceph-rbd"      <2>
    class: "cluster01-pool1"      <3>
  }
  "ceph-c01p02": {
    provider: "csi-ceph-rbd"
    class: "cluster01-pool1"
  }
  "ceph-c02p01": {
    provider: "csi-ceph-rbd"
    class: "cluster01-pool1"
  }
}
----
<1> A user-friendly name for the volume type. Users will use this name when selecting the type of
    volume resources they register.
<2> Name of the storage provider addon
<3> Name of the class of the storage provider that will be used to back this volume type


== Ceph utilities for debugging

Accessing a Ceph pool for testing or degugging purposes can be achieved with the `ceph` CLI tool.

To install `ceph` package:
----
$ sudo apt install ceph
----

Then create a `ceph.conf` file and a `ceph.keyring` file.

----
$ nano ceph.conf

fsid = 12345678-1234-1234-1234-222222222222
mon host = [v2:192.168.0.10:3300/0,v1:192.168.0.10:6789/0] \
           [v2:192.168.0.11:3300/0,v1:192.168.0.11:6789/0] \
           [v2:192.168.0.12:3300/0,v1:192.168.0.12:6789/0]
auth_client_required = cephx
----

----
$ nano ceph.keyring

[<POOL_USERNAME>]
	key = <POOL_PASSWORD>
----

The `ceph` CLI (and related CLIs, like `rbd`) can now be used by pointing at the files previously
created-

To get the cluster ID:
----
$ ceph \
  --conf ceph.conf \
  --keyring ceph-cluster-config/ceph.keyring \
  --id rbd.kumori \
  fsid
----

To get the cluster monitor list and information:
----
$ ceph \
  --conf ceph.conf \
  --keyring ceph-cluster-config/ceph.keyring \
  --id rbd.kumori \
  mon dump
----

To list the Ceph pool RBD volumes:
----
$ rbd \
  --conf ceph.conf \
  --keyring ceph.keyring \
  --pool rbd.kumori \
  --id rbd.kumori \
  ls
----

== Node failure scenario

One of the advantages of using Ceph RBD for providing storage is that, in most cases, the volumes
will reside outside the actual cluster nodes in a Ceph cluster deployed on different machines.
Nevertheless, if the node where a Pod using a volume is running fails, that Pod must be reloacted
to a different node and access to the volume must be set up in the new node.

This section describes what happens in such scenario, then proposes some manual actions to minimize
the downtime of the Pod.

=== What happens when the Node fails

We have tested some Node failure scenarios to observe how Pod rescheduling behaves when Pod are 
using a Ceph RBD volume. This is a summary of what happens:

* Pod A is running on Node w03 and using a Ceph RBD persistent volume.
* Node w03 is stopped (shutdown).
* Some seconds later, Node w03 is marked as *`NotReady`*.
* Since Pod is from a StatefulSet, it is not evicted automatically.
* We *delete it manually* with: +
  `kubectl -n kumori delete pods <pod A> --grace-period=0 --force`
* A *new Pod* is automatically scheduled to a healthy Node (w01) but *remains in
  `ContainerCreating`  status, due to the volume (PV) being attached to a different Node* (w03)
  via a `VolumeAttachment` object. +
  *Error*: `attachdetach-controller:  Multi-Attach error for volume "pvc-..." -`  
  `Volume is already exclusively attached to one node and can't be attached to another`
* After *6 minutes, the `VolumeAttachment` object* (linking the PV and w03) *is deleted automatically*.
* Immediately, a new `VolumeAttachment` (PV - w01) is created and *the new Pod creation progresses*.
* The new Pod has the volume mounted and runs normally.


The 6 minute delay for detaching the volume from the Node is due to a Kube-Controller configuration
setting *`maxWaitForUnmountDuration`* that is currently not configurable.

References about the 6 minute detach delay:

* A proposal to make maxWaitForUnmountDuration configurable (default is 6 minutes) +
  https://github.com/kubernetes/kubernetes/issues/68086
* https://github.com/kubernetes/kubernetes/issues/65392
* https://github.com/kubernetes/kubernetes/issues/100555#issuecomment-807913671
* https://github.com/longhorn/longhorn/issues/3584#issuecomment-1029784558

NOTE: To avoid the 6 minute wait, the volumeattachment object can be manually deleted. This is
described later in this document.

*The blocking of the new attachment* of the volume on a different node is due to the volume having
an *`AccessMode` of RWO* (`ReadWriteOnce`) which means *a volume can only mounted by one Node at a
time*. Another access mode (`RWX ReadWriteMany`) could be used and would probably result in the
volume being attached to the second Node even if the volume is still attached to the first one.
However, `RWX` must be supported by the CSI provider and the filesystem used, which is not always
the case. For example:

* *CSI Openstack Cinder* can only support `RWX` if the underlying Openstack offers a multi attach
  volume type (ours - ITI and OVH - don't).
* *CSI NFS* does support `RWX`, since it uses its own filesystem (NFS).
* *CSI Ceph RBD* does support `RWX` only in block mode, not with filesystems.
* for all the above, keep in mind that *ext4 and XFS are not cluster-aware filesystems* and
  therefore might end up causing data corruption.


=== Manual deletion of `VolumeAttachment` after a Node failure

We have tested and confirmed that, after a Node fails, *manually deleting the Pod and the
`VolumeAttachement` objects will allow for the new Pod to be created immediately in a different
Node and the volume attached to that Node*. By performing this manual action, we can bypass
Kube-Controller's 6-minute wait for automatic deletion of the VolumeAttachment.

The test steps are:

* Pod A is running on Node w03 and using a Ceph RBD persistent volume.
* Node w03 is stopped (shutdown).
* Some seconds later, Node w03 is marked as *`NotReady`*.
* Since Pod is from a StatefulSet, it is not evicted automatically.
* We *delete it manually* with: +
  `kubectl -n kumori delete pods <pod A> --grace-period=0 --force`
* A *new Pod* is automatically scheduled to a healthy Node (w01) but *remains in
  `ContainerCreating`  status, due to the volume (PV) being attached to a different Node* (w03)
  via a `VolumeAttachment` object. +
  *Error*: `attachdetach-controller:  Multi-Attach error for volume "pvc-..." -`  
  `Volume is already exclusively attached to one node and can't be attached to another`
* We manually delete the `VolumeAttachment` linking the PV to w03: +
  `kubectl delete volumeattachment <volumeAttachmentName>`
* Immediately, a new `VolumeAttachment` (PV - w01) is created and *the new Pod creation progresses*.
* The new Pod has the volume mounted and runs normally.








