= Kumori cluster: Using NFS storage
Kumori Systems S.L. <info@kumori.systems>
v1.4.0, Feb 2024
:title-page:
:compat-mode:
:doctype: article
:experimental:
:icons: font
:sectanchors:
:sectlinks:
:sectnums:
:toc:
:toclevels: 3
:imagesdir: ./assets/images

Kumori Cluster can be configured to provide volumes backed by an NFS server. +
This document describes how to configure the CSI NFS addon in Kumori Clusters.


== NFS configuration (previous step)

Prior to using the CSI NFS provider, access to an NFS server must be available.

The necessary data to gather from the NFS is:

* NFS server URL
* Shared path to connect to


== Cluster configuration

=== CSI NFS provider configuration

To enable the CSI NFS storage provider in a Kumori Cluster, it must be enabled when configuring the
cluster. In the `config.cue` file the necessary NFS server configuration settings must be provided,
and a list of classes configured.

[source%autofit,cue]
----
"csi-nfs": {
  enabled: true                      <1>

  // Classes of storage that will be available for backing user volume types
  classes: {                         <2>
    "persistent": {                  <3>
      configuration: {               <4>
        server: "192.168.100.10"     <5>
        sharePath: "/"               <6>
      }
      properties: "persistent"       <7>
    }
    "persistent-shared": {
      configuration: {
        server: "192.168.100.10"
        sharePath: "/"
      }
      properties: "persistent,shared"
    }
    "volatile": {
      configuration: {
        server: "192.168.100.10"
        sharePath: "/"
      }
      properties: "volatile"
    }
  }
}
----
<1> Enable or disable the installation of the CSI NFS storage provider in the cluster
<2> List of classes of storage that will be connected to the CSI NFS provider
<3> A name of the class, for referring to it when defining the cluster volume types
<4> Configuration section (CSI NFS-specific)
<5> For each class, the NFS server URL
<6> For each class, the path of the NFS share to connect to
<7> Properties section. Available properties are: `persistent` or `volatile` (mututally exclusive),
    and `shared`. These properties determine what the life-cycle of the provided volumes will be. +


=== Cluster volume types

Besides configuring CSI providers and the classes of storage they offer, a list of volume types
that will be available for users must be configured.

[source%autofit,cue]
----
// Configuration of the volume types that will be available to users
// Each volume type has a name (the map key) and must include:
// - a provider: be one of the configured Storage providers or "" (volumes
//   will be provided using temporary directories)
// - a class: a class configured in the selected provider. If no provider
//   is set, it should be set to "" (it will be ignored)
volumeTypes: {
  "nfs-persistent": {            <1>
    provider: "csi-nfs"          <2>
    class: "persistent"          <3>
  }
  "nfs-persistent-shared": {
    provider: "csi-nfs"
    class: "persistent-shared"
  }
  "nfs-volatile": {
    provider: "csi-nfs"
    class: "volatile"
  }
}
----
<1> A user-friendly name for the volume type. Users will use this name when selecting the type of
    volume resources they register.
<2> Name of the storage provider addon
<3> Name of the class of the storage provider that will be used to back this volume type


== Tools for debugging

For debugging NFS volumes, connecto to the NFS server and browse the shared directories. Each
volume corresponds to a subdirectory in the share path.
