= Spreading configuration to roles

A Service application specification includes a config section, intended
to configure all aspects of a service when the parameters are set on deployment.

We call the process of computing role configuration based on service configuration
the _spread_ of the service.

Defining how the spread has to proceed can be easily accomplished when specifying a role. 
There are three main aspects to take care of: scale, resilience and the parameter/resource
configuration.

Setting `resilience` based on the values of parameters set on deployment can be done quite
easily:

[source,cue]
----
role: myrole: config: resilience: description.config.parameter.maxfailures

----

In this straightforward example, we simply use the value of parameter `maxfailures` directly.

Fixing the rest of the configuration for the component behind a role is readily accomplished too:

[source,cue]
----
role: myrole: artifact: myComponentManifest
role: myrole: config: { 
  parameter: color: description.config.parameter.background
}
----

This simple example shows how to spread the `background` parameter of the service
to a hypothetical `color` parameter of the artifact implementing `myrole`.

Using CUE's expressiveness and built-in packages, an integrator can use more complex logic 
to encode more powerful transformations.

Like the above, the scale can also be spread easily. In the case of a component role,
only the hsize field of scale is mandatory, although the `qos` field may be conveniently
set so that the component spec logic can compute parameters for its various sizes.
