package component

import k "kumori.systems/kumori/kmv3_0_1"

#Artifact:  k.#Artifact & {

  ref: {
    domain: "examples.kumori.systems"
    name: "worker"
    version: [0,0,2]
    kind: "component"
  }

  description: {

    srv: {
      server: restapiserver: {
        protocol: "http"
        port:     8080
      }
      duplex: hello: {
        protocol: "tcp"
        port:     8081
      }
    }

    config: {
      parameter: {
        common_json: {
          commonParameter1: string
          commonParameter2: number
        }
      }
    }

    size: {
      badwidth: {size: 1000 unit: "M"} 
    }

    code: worker: k.#Container & {
      name: "worker"
      size: cpu: {size: 1000, unit: "m"}
      size: memory: {size: 4 unit: "G"}

      image: {
        hub: {
          name: "kumori"
          secret: ""
        }
        tag: "calculatorworker:0.0.7"
      }
      mapping: {
        filesystem: [
          {
            path: "/config/common_json.json"
            data: value: config.parameter.common_json
            format: "json"
          }
        ]
        env: {
          RESTAPISERVER_PORT_ENV: value: "\(srv.server.restapiserver.port)"
          HELLO_PORT_ENV: value: "\(srv.duplex.hello.port)"
        }
      }
    }
  }
}